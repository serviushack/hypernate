# hypernate

Get more out of hibernation.

# Features

* Reboot to a different OS instead of shutting down
* Optionally free up memory by
  * stopping Docker containers during hibernation
  * stopping Systemd units during hibernation

## Building

Simply run:

    cargo build

## Usage

Assume you have this grub boot menu:

```
┌────────────────┐
│ Linux          │
│ Linux fallback │
│ Windows        │
└────────────────┘
```

Then, to reboot to Windows after hibernating, use this:

    hypernate --grub-reboot-to 2
