# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.2.0] - 2024-01-25
### Added
- Support Hyprland.
- Added `--wait-on-error` flag to not exit when an error occured.
- Added `--mute-pulseaudio` flag to mute all audio outputs before hibernation.

## [1.1.0] - 2023-09-28
### Added
- Support switching off screens.

### Changed
- Updated dependencies.

## [1.0.0] - 2019-02-24
### Added
- Stop Docker containers before hibernation and start them again after wakeup.
- Stop systemd services before hibernation and start them again after wakeup.

### Changed
- **BREAKING**: Command line parameters work in the usual way now. Instead of just passing the menu entry to reboot to use the `--grub-reboot-to` paramter.
- **BREAKING**: When no `--grub-reboot-to` parameter is specified no reboot is initiated after hibernation.

## 0.9.0 - 2019-02-16
### Changed
- Renamed the project from *hibereboot* to *hypernate* to emphasize it's for more than just rebooting.

[Unreleased]: https://gitlab.com/serviushack/hypernate/compare/1.0.0...master
[1.0.0]: https://gitlab.com/serviushack/hypernate/compare/0.9.0...1.0.0
