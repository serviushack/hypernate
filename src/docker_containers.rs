use commands;

pub struct DockerContainerStopper {
    container: String,
}

impl DockerContainerStopper {
    pub fn new(container: &str) -> Result<DockerContainerStopper, String> {
        info!("Stopping docker container {}", container);
        match commands::docker_stop(container) {
            Ok(_) => Ok(DockerContainerStopper {
                container: container.to_string(),
            }),
            Err(message) => Err(message),
        }
    }
}

impl Drop for DockerContainerStopper {
    fn drop(&mut self) {
        info!("Starting docker container {}", &self.container);
        match commands::docker_start(&self.container) {
            Ok(_) => {}
            Err(message) => {
                error!("{}", message);
            }
        }
    }
}
