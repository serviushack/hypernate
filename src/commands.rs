use serde::Deserialize;
use std::process::Command;
use std::str;

/// Use `systemctl --dry-run reboot` to test if rebooting is allowed.
pub fn is_reboot_allowed() -> Result<bool, String> {
    let mut command = Command::new("systemctl");
    command.args(["--dry-run", "reboot"]);
    match command.status() {
        Ok(status) => Ok(status.success()),
        Err(err) => Err(format!(
            "Failed calling {}: {}",
            "systemctl --dry-run reboot", err
        )),
    }
}

/// Use `grub-reboot` to set the grub menu entry to preselect on the next boot.
pub fn set_reboot_entry(entry: &str) -> Result<(), String> {
    let mut command = Command::new("grub-reboot");
    command.arg(entry);
    call_command("grub-reboot", command)
}

/// Initiate the hibernation process by calling `systemctl hibernate`.
pub fn trigger_hibernate() -> Result<(), String> {
    let mut command = Command::new("systemctl");
    command.arg("hibernate");
    call_command("systemctl hibernate", command)
}

/// Stop a docker container.
pub fn docker_stop(container: &str) -> Result<(), String> {
    let mut command = Command::new("docker");
    command.args(["stop", container]);
    call_command("docker stop", command)
}

/// Start a docker container.
pub fn docker_start(container: &str) -> Result<(), String> {
    let mut command = Command::new("docker");
    command.args(["start", container]);
    call_command("docker start", command)
}

/// Stop a systemd unit.
pub fn systemctl_stop(unit: &str) -> Result<(), String> {
    let mut command = Command::new("systemctl");
    command.args(["stop", unit]);
    call_command("systemctl stop", command)
}

/// Start a systemd unit.
pub fn systemctl_start(unit: &str) -> Result<(), String> {
    let mut command = Command::new("systemctl");
    command.args(["start", unit]);
    call_command("systemctl start", command)
}

/// Is DPMS enabled?
pub fn dpms_enabled() -> Result<bool, String> {
    let mut command = Command::new("xset");
    command.args(["q"]);
    let output = call_command_capture_output("xset q", command)?;

    Ok(output.contains("DPMS is Enabled"))
}

/// Turn screens off.
pub fn dpms_screens_off() -> Result<(), String> {
    let mut command = Command::new("xset");
    command.args(["dpms", "force", "off"]);
    call_command("xset dpms force off", command)
}

/// Turn screens on.
pub fn dpms_screens_on() -> Result<(), String> {
    let mut command = Command::new("xset");
    command.args(["dpms", "force", "on"]);
    call_command("xset dpms force on", command)
}

/// Turn screens off for Hyprland.
pub fn hyprland_dpms_screens_off() -> Result<(), String> {
    let mut command = Command::new("hyprctl");
    command.args(["dispatch", "dpms", "off"]);
    call_command("hyprctl dispatch dpms off", command)
}

/// Turn screens on for Hyprland.
pub fn hyprland_dpms_screens_on() -> Result<(), String> {
    let mut command = Command::new("hyprctl");
    command.args(["dispatch", "dpms", "on"]);
    call_command("hyprctl dispatch dpms on", command)
}

/// Disable DPMS.
pub fn disable_dpms() -> Result<(), String> {
    let mut command = Command::new("xset");
    command.args(["-dpms"]);
    call_command("xset -dpms", command)
}

#[derive(Deserialize)]
struct PulseAudioSink {
    name: String,
    mute: bool,
}

/// List unmuted sinks.
pub fn pactl_list_unmuted_sinks() -> Result<Vec<String>, String> {
    let mut command = Command::new("pactl");
    command.args(["--format", "json", "list", "sinks"]);
    let sinks: Vec<PulseAudioSink> = serde_json::from_str(&call_command_capture_output(
        "pactl --format json list sinks",
        command,
    )?)
    .map_err(|e| e.to_string())?;

    Ok(sinks
        .into_iter()
        .filter(|sink| !sink.mute)
        .map(|sink| sink.name)
        .collect())
}

/// Mute sink.
pub fn pactl_mute(sink: &str) -> Result<(), String> {
    let mut command = Command::new("pactl");
    command.args(["set-sink-mute", sink, "1"]);
    call_command("pactl set-sink-mute", command)
}

/// Unmute sink.
pub fn pactl_unmute(sink: &str) -> Result<(), String> {
    let mut command = Command::new("pactl");
    command.args(["set-sink-mute", sink, "0"]);
    call_command("pactl set-sink-mute", command)
}

/// Execute a prepared command and nicely report in case of failure.
fn call_command(name: &str, mut command: ::std::process::Command) -> Result<(), String> {
    match command.status() {
        Err(err) => Err(format!("Failed calling {}: {}", name, err)),
        Ok(status) if !status.success() => Err(match status.code() {
            Some(code) => format!("{} failed with status code: {}", name, code),
            None => format!("{} terminated by signal", name),
        }),
        Ok(_) => Ok(()),
    }
}

/// Execute a prepared command returning the output and nicely report in case of failure.
fn call_command_capture_output(
    name: &str,
    mut command: ::std::process::Command,
) -> Result<String, String> {
    match command.output() {
        Err(err) => Err(format!("Failed calling {}: {}", name, err)),
        Ok(output) if !output.status.success() => Err(match output.status.code() {
            Some(code) => format!("{} failed with status code: {}", name, code),
            None => format!("{} terminated by signal", name),
        }),
        Ok(output) => Ok(str::from_utf8(&output.stdout).unwrap().to_owned()),
    }
}
