use commands;

pub struct SystemdUnitStopper {
    unit: String,
}

impl SystemdUnitStopper {
    pub fn new(unit: &str) -> Result<SystemdUnitStopper, String> {
        info!("Stopping systemd unit {}", unit);
        match commands::systemctl_stop(unit) {
            Ok(_) => Ok(SystemdUnitStopper {
                unit: unit.to_string(),
            }),
            Err(message) => Err(message),
        }
    }
}

impl Drop for SystemdUnitStopper {
    fn drop(&mut self) {
        info!("Starting systemd unit {}", &self.unit);
        match commands::systemctl_start(&self.unit) {
            Ok(_) => {}
            Err(message) => {
                error!("{}", message);
            }
        }
    }
}
