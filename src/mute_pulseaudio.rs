use commands;

pub struct MutePulseaudio {
    sinks: Vec<String>,
}

impl MutePulseaudio {
    pub fn new() -> Result<MutePulseaudio, String> {
        debug!("Listing PulseAudio sinks");
        match commands::pactl_list_unmuted_sinks() {
            Ok(sinks) => {
                for sink in &sinks {
                    info!("Muting sink {}", sink);
                    commands::pactl_mute(sink)?;
                }
                Ok(MutePulseaudio { sinks })
            }
            Err(message) => Err(message),
        }
    }
}

impl Drop for MutePulseaudio {
    fn drop(&mut self) {
        for sink in &self.sinks {
            info!("Unmuting sink {}", sink);
            match commands::pactl_unmute(sink) {
                Ok(_) => {}
                Err(message) => {
                    error!("{}", message);
                }
            }
        }
    }
}
