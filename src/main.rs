#[macro_use]
extern crate clap;
extern crate hypernate;
extern crate stderrlog;

#[macro_use]
extern crate log;

use std::io;

use clap::{Arg, Command};

fn main() {
    let matches = Command::new(crate_name!())
        .about(crate_description!())
        .version(crate_version!())
        .arg(
            Arg::new("verbosity")
                .short('v')
                .action(clap::ArgAction::Count)
                .help("Increase message verbosity"),
        )
        .arg(
            Arg::new("quiet")
                .short('q')
                .action(clap::ArgAction::SetTrue)
                .help("Silence all output"),
        )
        .arg(
            Arg::new("grub-reboot-to")
                .long("grub-reboot-to")
                .short('r')
                .value_name("menu entry")
                .help("The GRUB menu entry to reboot to"),
        )
        .arg(
            Arg::new("docker-stop")
                .short('d')
                .long("docker-stop")
                .value_name("container")
                .num_args(0..)
                .help("Stop a container for hibernation"),
        )
        .arg(
            Arg::new("systemd-unit-stop")
                .short('s')
                .long("systemd-unit-stop")
                .value_name("unit")
                .num_args(0..)
                .help("Stop a systemd unit for hibernation"),
        )
        .arg(
            Arg::new("dpms-turn-screens-off")
                .short('b')
                .long("dpms-turn-screens-off")
                .action(clap::ArgAction::SetTrue)
                .help("Turn screens off via DPMS"),
        )
        .arg(
            Arg::new("wait-on-error")
                .short('w')
                .long("wait-on-error")
                .action(clap::ArgAction::SetTrue)
                .help("Pause before exiting when an error occurred"),
        )
        .arg(
            Arg::new("mute-pulseaudio")
                .short('m')
                .long("mute-pulseaudio")
                .action(clap::ArgAction::SetTrue)
                .help("Mute all PulseAudio sinks before hibernation"),
        )
        .get_matches();

    // Configure logging
    let verbose = matches.get_count("verbosity") as usize;
    let quiet = matches.get_flag("quiet");
    stderrlog::new()
        .module(module_path!())
        .quiet(quiet)
        .verbosity(verbose)
        .init()
        .unwrap();

    if let Err(e) = hypernate::run(
        matches.get_one("grub-reboot-to"),
        matches
            .get_many("docker-stop")
            .unwrap_or_default()
            .collect(),
        matches
            .get_many("systemd-unit-stop")
            .unwrap_or_default()
            .collect(),
        matches.get_flag("dpms-turn-screens-off"),
        matches.get_flag("mute-pulseaudio"),
    ) {
        error!("{}", e);
        if matches.get_flag("wait-on-error") {
            println!("[Press enter to continue]");
            let mut _buffer = String::new();
            io::stdin().read_line(&mut _buffer).unwrap();
        }
    }
}
