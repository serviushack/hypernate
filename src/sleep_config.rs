use std::fs;
use std::fs::File;
use std::io::prelude::Write;

/// Configuration for systemd-hibernate.service
///
/// According to `man 5 systemd-sleep.conf`
/// this will write 'reboot' to `/sys/power/disk`.
static REBOOT_CONFIG: &[u8] = br#"[Sleep]
HibernateMode=reboot
"#;

/// Configuration file to use.
///
/// Don't try to modify `/etc/systemd/sleep.conf` which might be user-maintained.
/// Instead use a dedicated configuration file.
static CONFIG_FILE_PATH: &str = "/etc/systemd/sleep.conf.d/99-hypernate.conf";

/// Directory for the configuration file.
///
/// This directory might not exist. It will be attempted to create it to save the configuration
/// file.
static CONFIG_DIR: &str = "/etc/systemd/sleep.conf.d/";

/// Keep the reboot configuration active.
///
/// The default for a hibernate should be a shutdown. Only keep the configuration file existent
/// (i.e. active) as long as this program is running.
pub struct SleepConfigAdjustment {
    config_file: File,
}

impl SleepConfigAdjustment {
    fn create_file() -> Result<File, String> {
        match File::create(CONFIG_FILE_PATH) {
            Err(ref err) if err.kind() == ::std::io::ErrorKind::NotFound => {
                // This error means the parent directory doesn't exist. Try creating it.
                if let Err(err2) = fs::create_dir(CONFIG_DIR) {
                    return Err(format!("Failed creating {}: {}", CONFIG_DIR, err2));
                }

                // Try creating the file again.
                match File::create(CONFIG_FILE_PATH) {
                    Err(err2) => Err(format!("Failed creating {}: {}", CONFIG_FILE_PATH, err2)),
                    Ok(file) => Ok(file),
                }
            }
            Err(err) => Err(format!("Failed creating {}: {}", CONFIG_FILE_PATH, err)),
            Ok(file) => Ok(file),
        }
    }

    /// Create the configuration file
    ///
    /// This might also create the necessary directory. It will place the configuration to reboot
    /// into the file.
    pub fn new() -> Result<SleepConfigAdjustment, String> {
        let mut instance = match SleepConfigAdjustment::create_file() {
            Ok(file) => SleepConfigAdjustment { config_file: file },
            Err(err) => {
                return Err(err);
            }
        };

        if let Err(err) = instance.config_file.write_all(REBOOT_CONFIG) {
            return Err(format!("Writing to {} failed: {}", CONFIG_FILE_PATH, err));
        }
        if let Err(err) = instance.config_file.flush() {
            return Err(format!("Flusing {} failed: {}", CONFIG_FILE_PATH, err));
        }

        Ok(instance)
    }
}

impl Drop for SleepConfigAdjustment {
    /// Truncate the configuration file
    fn drop(&mut self) {
        self.config_file.set_len(0).expect("failed to truncate");
    }
}
