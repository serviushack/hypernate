use commands;

pub struct ScreensOff {
    hyprland: bool,
    was_dpms_enabled: bool,
}

impl ScreensOff {
    pub fn new() -> Result<ScreensOff, String> {
        info!("Querying DPMS state");

        let hyprland = std::env::var("HYPRLAND_INSTANCE_SIGNATURE").is_ok();
        let was_dpms_enabled = if hyprland {
            false // On Hyprland automatic screen blanking isn't handled via DMS.
        } else {
            commands::dpms_enabled()?
        };
        let screen_off_function = if hyprland {
            commands::hyprland_dpms_screens_off
        } else {
            commands::dpms_screens_off
        };

        info!("Turning screens off");
        match screen_off_function() {
            Ok(_) => Ok(ScreensOff {
                hyprland,
                was_dpms_enabled,
            }),
            Err(message) => Err(message),
        }
    }
}

impl Drop for ScreensOff {
    fn drop(&mut self) {
        info!("Turning screens on");
        if self.hyprland {
            if let Err(message) = commands::hyprland_dpms_screens_on() {
                error!("{}", message);
            }
        } else {
            match commands::dpms_screens_on() {
                Ok(_) => {
                    if !self.was_dpms_enabled {
                        info!("Disabling DPMS again");
                        if let Err(message) = commands::disable_dpms() {
                            error!("{}", message);
                        }
                    }
                }
                Err(message) => {
                    error!("{}", message);
                }
            }
        }
    }
}
